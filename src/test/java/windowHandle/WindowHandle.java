package windowHandle;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WindowType;

public class WindowHandle {
    public static String handleAndSwitch(WebDriver driver){
        driver.switchTo().newWindow(WindowType.TAB);
        return driver.getWindowHandle();
    }
}
