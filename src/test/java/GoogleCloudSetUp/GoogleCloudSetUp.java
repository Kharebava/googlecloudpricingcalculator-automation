package GoogleCloudSetUp;

import GoogleCloudPages.CalculationPage;
import GoogleCloudPages.CloudHomePage;
import GoogleCloudPages.YopMailPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import windowHandle.WindowHandle;

import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertTrue;

public class GoogleCloudSetUp {
    WebDriver driver;

    CloudHomePage objCloudHomePage;
    CalculationPage objCalculationPage;
    YopMailPage objYopMailPage;

    @BeforeTest
    public void setup(){
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://cloud.google.com/?hl=en");
    }

    @Test(priority = 1)
    public void navigateToHomePage(){
        objCloudHomePage = new CloudHomePage(driver);
        objCloudHomePage.clickOnSearch("Google Cloud Pricing Calculator");
        objCloudHomePage.clickOnResult();
    }

    @Test(priority = 2)
    public void calculatePrice(){
        objCalculationPage = new CalculationPage(driver);
        objCalculationPage.switchToCalculator()
                .clickComputeEngine()
                .addInstances(String.valueOf(4))
                .setSeries()
                .selectMachineType()
                .addGPU()
                .ssdSelection()
                .location()
                .committedUsageSelection()
                .addToEstimate();

        assertTrue(objCalculationPage.totalCost());
        String calculatedPrice = objCalculationPage.estimatedPrice();

        String originalWindowHandle = driver.getWindowHandle();
        String secondWindow = WindowHandle.handleAndSwitch(driver);

        driver.get("https://yopmail.com/");
        objYopMailPage = new YopMailPage(driver);
        objYopMailPage.generateEmail().copyEmail();

        driver.switchTo().window(originalWindowHandle);
        objCalculationPage.switchToCalculator()
                .emailEstimate()
                .enterEmail()
                .clickSendEmail();

        driver.switchTo().window(secondWindow);

        objYopMailPage.closeAd()
                .checkInbox()
                .clickRefresh();
        String receivedPrice = objYopMailPage.estimatedPrice();

        SoftAssert softAssert= new SoftAssert();
        softAssert.assertEquals(calculatedPrice, receivedPrice);
        softAssert.assertAll();

    }
    @AfterTest
    public void exitWindow() {
        driver.quit();
    }

}
