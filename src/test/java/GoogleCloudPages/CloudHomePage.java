package GoogleCloudPages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CloudHomePage {
    WebDriver driver;

    @FindBy(xpath="//input[@name='q'][@placeholder='Search']")
    WebElement search;
    @FindBy(xpath = "//a[@class='gs-title' and contains(@href, 'https://cloud.google.com/products/calculator')]")
    WebElement searchResult;

    public CloudHomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public CloudHomePage clickOnSearch(String input){
        search.click();
        search.sendKeys(input);
        search.sendKeys(Keys.RETURN);
        return this;
    }

    public CalculationPage clickOnResult(){
        searchResult.click();
        return new CalculationPage(driver);
    }
}
