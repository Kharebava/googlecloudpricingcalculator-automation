package GoogleCloudPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class YopMailPage {
    WebDriver driver;

    @FindBy(xpath = "//a[@href='email-generator']")
    WebElement EmailGenerateButton;
    @FindBy(id="cprnd")
    WebElement copyButton;
    @FindBy(xpath = "//*[text()='Check Inbox']")
    WebElement intboxButton;
    @FindBy(id="refresh")
    WebElement refreshButton;
    @FindBy(xpath = "//td/h3[text()='USD 1,081.20']")
    WebElement monthlyCost;
    @FindBy(id="aswift_3")
    WebElement adFirstIframe;
    @FindBy(id="ad_iframe")
    WebElement adCloseBtnIframe;
    @FindBy(xpath="//div[@id='dismiss-button']")
    WebElement adDismiss;
    @FindBy(id="ifmail")
    WebElement inboxIframe;

    public YopMailPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public YopMailPage generateEmail(){
//        directly opening generated email page via link, because when using button sometimes
//        pop up ad is displayed on the window and to avoid further problems and difficulties
//        I just used this way which works perfectly fine;

        driver.get("https://yopmail.com/email-generator");
        return this;
    }
    public YopMailPage copyEmail(){
        copyButton.click();
        return this;
    }
    public YopMailPage closeAd(){
        if(adFirstIframe.isDisplayed()){
            driver.switchTo().frame(adFirstIframe);
            driver.switchTo().frame(adCloseBtnIframe);
            adDismiss.click();
            driver.switchTo().defaultContent();
        }
        return this;
    }

    public YopMailPage checkInbox(){
        intboxButton.click();
        return this;
    }
    public YopMailPage clickRefresh(){
        refreshButton.click();
        return this;
    }
    public String estimatedPrice(){
        driver.switchTo().frame(inboxIframe);
        return monthlyCost.getText();
    }

}
