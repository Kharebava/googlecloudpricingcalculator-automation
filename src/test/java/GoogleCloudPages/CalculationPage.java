package GoogleCloudPages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class CalculationPage {
    WebDriver driver;

    @FindBy(xpath = "//article[@id='cloud-site']/devsite-iframe/iframe")
    WebElement firstIframe;
    @FindBy(xpath = "//iframe[@id='myFrame']")
    WebElement secondIframe;

    @FindBy(id="tab-item-1")
    WebElement computeEngine;
    @FindBy(xpath="//input[@ng-model='listingCtrl.computeServer.quantity']")
    WebElement numOfInstances;
    @FindBy(id = "select_124")
    WebElement series;
    @FindBy(id = "select_option_221")
    WebElement seriesN1;
    @FindBy(id ="select_126")
    WebElement machineType;
    @FindBy(id = "select_option_470")
    WebElement selectMT;
    @FindBy(xpath = "//md-checkbox[@ng-model='listingCtrl.computeServer.addGPUs']")
    WebElement addGPU;
    @FindBy(id="select_506")
    WebElement GPUType;
    @FindBy(id="select_option_513")
    WebElement chooseGPUType;
    @FindBy(id = "select_508")
    WebElement numOfGPUs;
    @FindBy(id="select_option_516")
    WebElement addNumOfGPUs;
    @FindBy(id="select_value_label_464")
    WebElement selectSSD;
    @FindBy(id="select_option_491")
    WebElement chooseSSD;
    @FindBy(id="select_132")
    WebElement locationDropDown;
    @FindBy(id="select_option_264")
    WebElement dataCenterLocation;
    @FindBy(id="select_value_label_98")
    WebElement usageDropDown;
    @FindBy(   id="select_option_137")
    WebElement committedUsage;
    @FindBy(xpath="//button[contains(@class, 'md-raised') and contains(@class, 'md-primary') and contains(@class, 'cpc-button') and contains(@class, 'md-button') and contains(@class, 'md-ink-ripple') and @type='button' and @ng-click='listingCtrl.addComputeServer(ComputeEngineForm);']")
    WebElement add;
    @FindBy(xpath = "//b[contains(text(),'Total Estimated Cost:') and contains(text(),'USD') and contains(text(),'per 1 month')]")
    WebElement cost;
    @FindBy(id= "Email Estimate")
    WebElement emailButton;
    @FindBy(xpath = "//input[@type='email']")
    WebElement emailField;
    @FindBy(xpath = "//button[contains(text(), 'Send Email')]")
    WebElement sendEmail;
    @FindBy(xpath = "//sub/b[contains(@class, 'ng-binding') and starts-with(text(), 'USD ')]")
    WebElement monthlyPrice;

    public CalculationPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public CalculationPage switchToCalculator(){
        driver.switchTo().frame(firstIframe);
        driver.switchTo().frame(secondIframe);
        return this;
    }
    public CalculationPage clickComputeEngine(){
        computeEngine.click();
        return this;
    }
    public CalculationPage addInstances(String input){
        numOfInstances.sendKeys(input);
        return this;
    }
    public CalculationPage setSeries(){
        series.click();
        waitElement(driver,seriesN1).click();
        return this;
    }
    public CalculationPage selectMachineType(){
        machineType.click();
        waitElement(driver, selectMT).click();
        return this;
    }
    public CalculationPage addGPU(){
        addGPU.click();
        GPUType.click();
        waitElement(driver, chooseGPUType).click();
        numOfGPUs.click();
        waitElement(driver, addNumOfGPUs).click();
        return this;
    }
    public CalculationPage ssdSelection(){
        selectSSD.click();
        waitElement(driver, chooseSSD).click();
        return this;
    }
    public CalculationPage location(){
        locationDropDown.click();
        waitElement(driver, dataCenterLocation).click();
        return this;
    }
    public CalculationPage committedUsageSelection(){
        usageDropDown.click();
        waitElement(driver, committedUsage).click();
        return this;
    }
    public CalculationPage addToEstimate(){
        add.click();
        return this;
    }
    public boolean totalCost(){
        return cost.isDisplayed();
    }
    public CalculationPage emailEstimate(){
        emailButton.click();
        return this;
    }
    public CalculationPage enterEmail(){
        waitElementprecense(driver, By.xpath("//input[@type='email']")).sendKeys(Keys.CONTROL, "v");
        return this;
    }
    public CalculationPage clickSendEmail(){
        sendEmail.click();
        return this;
    }
    public String estimatedPrice(){
        return monthlyPrice.getText();
    }



    private static WebElement waitElement(WebDriver driver,WebElement element) {
        return new WebDriverWait(driver, Duration.ofSeconds(15))
                .until(ExpectedConditions.elementToBeClickable(element));
    }
    private static WebElement waitElementprecense(WebDriver driver,By by) {
        return new WebDriverWait(driver, Duration.ofSeconds(15))
                .until(ExpectedConditions.presenceOfElementLocated(by));
    }
}
